package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

//==========================================================CLASSE DE CONTROLE DE PAGINAS=======================================================================
@Controller  
public class ControllerPages {

     @GetMapping(value = "/")  
     public ModelAndView exibirHome(){ 
          return new ModelAndView("Home");
     }
     
     @GetMapping(value = "/index")
     public ModelAndView exibirIndex(){  
          return new ModelAndView("Index");
     }
     
     @GetMapping(value = "/index2")
     public ModelAndView exibirIndex2(){  
          return new ModelAndView("Index2");
     }
}
