package controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import domain.User;
//========================================================CLASSE DE INSERÇÃO DE USUARIO=======================================================================

public class ControllerInsertUser {

	public void insert(User user) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("teste");

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(user);
		em.getTransaction().commit();

		em.close();
		emf.close();

	}
}
