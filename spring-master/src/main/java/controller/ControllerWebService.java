package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import domain.Course;
import domain.User;

//=============================================================CLASSE CONTROLE DO WEB SERVICE=======================================================================
@RestController
public class ControllerWebService {
	//==============VARIAVEL===================
	private Map<Integer, Course> cursos;
	private Map<Integer, User> usuarios;

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/cursos", method = RequestMethod.GET)
	//==============RETORNA UM JSON CONTENDO OS CURSOS===================
	public ResponseEntity<List<Course>> listar() {
		cursos = new HashMap<Integer, Course>();

		Course c1 = new Course(1, "Workshop Rest", "24hs");
		Course c2 = new Course(2, "Workshop Spring MVC", "24hs");
		Course c3 = new Course(3, "Desenvolvimento Web com JSF 2", "60hs");

		cursos.put(1, c1);
		cursos.put(2, c2);
		cursos.put(3, c3);
		return new ResponseEntity<List<Course>>(new ArrayList<Course>(cursos.values()), HttpStatus.OK);
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/usuarios", method = RequestMethod.GET)
	//==============RETORNA UM JSON CONTENDO OS USUARIOS DO BANCO DE DADOS===================
	public ResponseEntity<List<User>> listarUsers() {
		usuarios = new HashMap<Integer, User>();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("teste");

		EntityManager em = emf.createEntityManager();

		ArrayList<User> teste = (ArrayList)em.createQuery("FROM " + User.class.getName()).getResultList();

		for(int i=0;i<teste.size();i++) {
			User usuario = new User();
			usuario.setId(teste.get(i).getId());
			usuario.setEmail(teste.get(i).getEmail());
			usuario.setFullname(teste.get(i).getFullname());
			usuario.setPassword(teste.get(i).getPassword());

			usuarios.put(i,usuario);
		}
		
		em.close();
		emf.close();

		return new ResponseEntity<List<User>>(new ArrayList<User>(usuarios.values()), HttpStatus.OK);
	}
}