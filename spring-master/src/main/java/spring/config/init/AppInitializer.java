package spring.config.init;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import spring.config.WebConfig;

//=========================================================classe de inicialização SPRING FRAMEWORK=================================================================== 

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {  
    @Override  
    protected Class<?>[] getRootConfigClasses() {  
         return null;  
    }  
    @Override  
    protected Class<?>[] getServletConfigClasses() {  
         return new Class<?>[] { WebConfig.class };  
    }
    
    //Caminho Raiz
    @Override  
    protected String[] getServletMappings() {  
         return new String[] { "/" };  
    }  
}
